package fr.unicaen.jee.tp1.servlet;

import fr.unicaen.jee.tp1.logic.*;

public class Addition extends AbstractCalculetteServlet {
			
	@Override
	protected double calcule(double gauche, double droite) throws CalculetteException {
		return CalculetteFactory.getCalculette().addition(gauche, droite);
	}

}
