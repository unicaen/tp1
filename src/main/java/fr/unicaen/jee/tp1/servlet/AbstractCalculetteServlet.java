package fr.unicaen.jee.tp1.servlet;

import java.io.*;
import javax.servlet.*;
import javax.servlet.http.*;
import fr.unicaen.jee.tp1.logic.*;

/**
 * Servlet générique de calculette. 
 * S'occupe de la récupération et de la vérification des arguments, ainsi que du retour du résultat
 * ou d'une erreur le cas échéant. Le calcul en lui-même est réalisé par la méthode abstraite calcule(),
 * à implémenter par chaque servlet concrète.
 */
abstract class AbstractCalculetteServlet extends HttpServlet {
		
	private static final String RESPONSE_ENCODING = "UTF-8";
	
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
			double gauche = getDoubleParameter(request, "gauche");
			double droite = getDoubleParameter(request, "droite");						
			double result = calcule(gauche, droite);
			
			response.setContentType("text/plain");
			response.setCharacterEncoding(RESPONSE_ENCODING);
			
			OutputStream output = response.getOutputStream();
			output.write(String.valueOf(result).getBytes(RESPONSE_ENCODING));
			output.flush();
		}
		catch(IllegalArgumentException e) {
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, e.getMessage());
		}				
		catch(CalculetteException e) {
			response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, e.getMessage());
		}
	}		
		
	
	/**
	 * Récupère un paramètre de type double.
	 * @param request La requête
	 * @param name Le nom du paramètre à récupérer
	 * @return La valeur du paramètre sous forme de double
	 * @throws IllegalArgumentException Si le paramètre est manquant ou mal formé
	 */
	protected static double getDoubleParameter(HttpServletRequest request, String name) throws IllegalArgumentException {		
		String param = request.getParameter(name);
		if(param == null)
			throw new IllegalArgumentException("Cet argument est requis: " + name);
		
		try {
			return Double.parseDouble(param);
		}
		catch(NumberFormatException e) {
			throw new IllegalArgumentException("Argument mal formé: " + param);
		}				
	}
	
	
	/**
	 * Réalise le calcul.
	 * @param gauche Opérande gauche
	 * @param droite Opérande droite
	 * @return Le résultat
	 * @throws CalculetteException En cas d'erreur interne pendant de calcul
	 * @throws IllegalArgumentException En cas de paramètre incorrect
	 */
	protected abstract double calcule(double gauche, double droite) throws CalculetteException, IllegalArgumentException;

}
