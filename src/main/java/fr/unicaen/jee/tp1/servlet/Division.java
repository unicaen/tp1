package fr.unicaen.jee.tp1.servlet;

import fr.unicaen.jee.tp1.logic.*;

public class Division extends AbstractCalculetteServlet {
			
	@Override
	protected double calcule(double gauche, double droite) throws CalculetteException, IllegalArgumentException {
		return CalculetteFactory.getCalculette().division(gauche, droite);
	}		

}
