package fr.unicaen.jee.tp1.logic;


public class CalculetteException extends Exception {
	
	public CalculetteException(Throwable t) {
		super(t);
	}
	
	public CalculetteException(String s) {
		super(s);
	}
	
}
