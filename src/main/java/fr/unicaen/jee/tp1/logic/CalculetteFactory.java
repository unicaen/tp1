package fr.unicaen.jee.tp1.logic;


/**
 * Factory de calculette
 */
public class CalculetteFactory {
		
	private static Calculette calculette = null;
	
	
	/**
	 * Crée une instance de Calculette. Cette méthode est ré-entrante.	 
	 * @return Une instance de Calculette
	 */
	public static synchronized Calculette getCalculette() throws CalculetteException {
		if(calculette == null)
			calculette = new DefaultCalculette();
		return calculette;
	}
	
}
