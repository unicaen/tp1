package fr.unicaen.jee.tp1.logic;


/**
 * Implémentation par défaut de l'interface Calculette.
 */
public class DefaultCalculette implements Calculette {

	@Override
	public double addition(double x, double y) {
		return x + y;
	}

	@Override
	public double soustraction(double x, double y) {
		return x - y;
	}

	@Override
	public double multiplication(double x, double y) {
		return x * y;
	}

	@Override
	public double division(double x, double y) throws IllegalArgumentException {
		if(y == 0)
			throw new IllegalArgumentException("Division par zéro");
		else
			return x / y;
	}
	
	
	
}
