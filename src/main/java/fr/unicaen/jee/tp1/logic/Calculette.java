package fr.unicaen.jee.tp1.logic;

/**
 * Contrat de base pour la logique métier de la calculette.
 */
public interface Calculette {
	
	/**
	 * Effectue une addition
	 * @param x Opérande gauche
	 * @param y Opérande droite
	 * @return Le résultat de l'addition de x et de y
	 * @throws CalculetteException En cas d'erreur de calcul
	 * @throws IllegalArgumentException En cas d'argument incompatible avec l'opération
	 */
	double addition(double x, double y) throws CalculetteException, IllegalArgumentException;
	
	
	/**
	 * Effectue une soustraction
	 * @param x Opérande gauche
	 * @param y Opérande droite
	 * @return Le résultat de la soustraction de y à x
	 * @throws CalculetteException En cas d'erreur de calcul
	 * @throws IllegalArgumentException En cas d'argument incompatible avec l'opération
	 */
	double soustraction(double x, double y) throws CalculetteException, IllegalArgumentException;
	
	
	/**
	 * Effectue une multiplication
	 * @param x Opérande gauche
	 * @param y Opérande droite
	 * @return Le résultat de la multiplication de x par y
	 * @throws CalculetteException En cas d'erreur de calcul
	 * @throws IllegalArgumentException En cas d'argument incompatible avec l'opération
	 */
	double multiplication(double x, double y) throws CalculetteException, IllegalArgumentException;
	
	
	/**
	 * Effectue une division
	 * @param x Opérande gauche
	 * @param y Opérande droite
	 * @return Le résultat de la division de x par y
	 * @throws CalculetteException En cas d'erreur de calcul
	 * @throws IllegalArgumentException En cas d'argument incompatible avec l'opération
	 */
	double division(double x, double y) throws CalculetteException, IllegalArgumentException;

}
