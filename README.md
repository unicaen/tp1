# TP1 #

L'objectif de ce TP est de se familiariser avec la couche « Servlet » de JEE, en l'utilisant pour créer un web- service de type REST. L'application en elle-même est délibérément simpliste, de façon à pouvoir de concentrer sur les sujet qui nous occupent, à savoir la création des servlets et leur déploiement dans un conteneur web.

Voir l'énoncé complet sur le [Moodle](http://foad2.unicaen.fr/moodle/course/view.php?id=24560).